import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;


/**
 * The simple RGB class, implementing algorithmical solutions for image processing problems
 *
 * With this class you can do ex. 2-D order-statistic filtering (ordfilt2 - Matlab's naming)
 *
 * <p>RGB rgb = new RGB();</p>
 *
 * First upload Image to WrtiableImage object
 *
 * <p>WritableImage image = rgb.loadImage("cameraman.png");</p>
 *
 * Then execute function
 *
 * <p>image = rgb.ordfilt2(image,3,10);</p>
 *
 * Voilà!
 *
 *
 * @author  KiboleHutnik
 * @version 1.0
 * @since   2018-05-21
 */
public class RGB extends PictureType implements ISpatialFiltration, PictureUtilities {
    String exceptionMsg = "Image must be RGB...";
    MyException newExc=new MyException(exceptionMsg);
    /**
     *  2-D order-statistic filtering
     * @param image       The picture on which you provide order-statistic filtering
     * @param choice Element to replace the target picture
     * @param maskSize The size of numeric matrix
     * @return Ordered-statistics filtering picture as WritableImage object
     */
    public WritableImage ordfilt2(WritableImage image, int choice, int maskSize) throws MyException
    {
        if (isRGB(image) == false) {
            throw newExc;
        }
        if(choice > maskSize*maskSize)
        {
            System.out.println("Please provide valid number");
            return null;
        }
        WritableImage writableImage = new WritableImage((int)image.getWidth(),(int)image.getHeight());

        for(int i = 0;i<writableImage.getHeight();i++)
        {
            for(int j =0;j<writableImage.getWidth();j++)
            {
                writableImage.getPixelWriter().setColor(j,i,sortAndChose(image,choice,maskSize,i,j));
            }
        }

        //saveImage(writableImage,name);

        return writableImage;
    }

    /**
     *      Local standard deviation of image
     * @param writableImage       The picture on which you provide standard deviation
     * @param tab Multidimensional, logical, or numeric array containing zeros and ones
     * @return Local standard deviation picture as WritableImage object
     */
    public WritableImage stdfilt(WritableImage writableImage, int[][] tab) throws MyException
    {
        if (isRGB(writableImage) == false) {
            throw newExc;
        }
        if(tab.length%2 == 0 || tab[0].length != tab.length) return null;
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());

        for (int i = tab.length/2; i < (int) writableImage.getHeight()-(tab.length/2); i++)
            for (int j = tab.length/2; j < (int) writableImage.getWidth()-(tab.length/2); j++)
                w.getPixelWriter().setColor(j, i, std(writableImage, tab, i, j));

        w = normalization(w);
        //saveImage(w, name);

        return w;
    }

    private Color std(WritableImage writableImage, int[][] mask, int x0, int y0)
    {
        Color tmp;
        int size = mask.length;
        double n = 0.0;
        double sredniaBlue = 0;
        double sredniaRed = 0;
        double sredniaGreen = 0;
        int x = 0;
        int y = 0;
        for (int i = x0 - size/2; i < x0 + size/2; i++) {
            for (int j = y0 - size / 2; j < y0 + size / 2; j++) {

                if(mask[y][x] >= 1) {
                    sredniaBlue += writableImage.getPixelReader().getColor(j, i).getBlue();
                    sredniaRed += writableImage.getPixelReader().getColor(j, i).getRed();
                    sredniaGreen += writableImage.getPixelReader().getColor(j, i).getGreen();
                    n++;

                }
                x++;
            }
            y++;
            x=0;
        }
        sredniaBlue /= n;
        sredniaRed /= n;
        sredniaGreen /= n;

        double skladowaRed = 0;
        double skladowaGreen = 0;
        double skladowaBlue = 0;
        for (int i = x0 - size/2; i <= x0 + size/2; i++) {
            for (int j = y0 - size / 2; j <= y0 + size / 2; j++) {
                skladowaRed += Math.pow(writableImage.getPixelReader().getColor(j, i).getRed() - sredniaRed, 2);
                skladowaGreen += Math.pow(writableImage.getPixelReader().getColor(j, i).getGreen() - sredniaGreen, 2);
                skladowaBlue += Math.pow(writableImage.getPixelReader().getColor(j, i).getBlue() - sredniaBlue, 2);
            }
        }
        skladowaBlue /= (n-1);
        skladowaRed /= (n-1);
        skladowaGreen /= (n-1);

        skladowaBlue = Math.sqrt(skladowaBlue);
        skladowaRed = Math.sqrt(skladowaRed);
        skladowaGreen = Math.sqrt(skladowaGreen);

        if(skladowaBlue > 1) skladowaBlue = 1.0;
        if(skladowaGreen > 1) skladowaGreen = 1.0;
        if(skladowaRed > 1) skladowaRed = 1.0;
        tmp = Color.color(skladowaRed, skladowaGreen, skladowaBlue);

        return tmp;
    }

    private WritableImage normalization(WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int)writableImage.getWidth(), (int)writableImage.getHeight());
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < (int) writableImage.getHeight(); i++){
            for (int j = 0; j < (int) writableImage.getWidth(); j++) {
                if(min > writableImage.getPixelReader().getColor(j, i).getBlue())
                    min = writableImage.getPixelReader().getColor(j, i).getBlue();
                if(min > writableImage.getPixelReader().getColor(j, i).getRed())
                    min = writableImage.getPixelReader().getColor(j, i).getRed();
                if(min > writableImage.getPixelReader().getColor(j, i).getGreen())
                    min = writableImage.getPixelReader().getColor(j, i).getGreen();

                if(max < writableImage.getPixelReader().getColor(j, i).getBlue())
                    max = writableImage.getPixelReader().getColor(j, i).getBlue();
                if(max < writableImage.getPixelReader().getColor(j, i).getRed())
                    max = writableImage.getPixelReader().getColor(j, i).getRed();
                if(max < writableImage.getPixelReader().getColor(j, i).getGreen())
                    max = writableImage.getPixelReader().getColor(j, i).getGreen();
            }
        }

        double wsp = 1.0/(max-min);

        for (int i = 0; i < (int) writableImage.getHeight(); i++) {
            for (int j = 0; j < (int) writableImage.getWidth(); j++) {
                Color color = Color.color((writableImage.getPixelReader().getColor(j, i).getRed()-min)*wsp,
                        (writableImage.getPixelReader().getColor(j, i).getGreen()-min)*wsp,
                        (writableImage.getPixelReader().getColor(j, i).getBlue()-min)*wsp);
                w.getPixelWriter().setColor(j,i,color);
            }
        }

        return w;
    }

    private Color sortAndChose(WritableImage image,int choice, int maskSize,int x0,int y0)
    {
        ArrayList red =new  ArrayList();
        ArrayList green =new  ArrayList();
        ArrayList blue =new  ArrayList();

        for (int i = x0; i < x0 + maskSize; i++) {
            for (int j = y0 ; j < y0 + maskSize; j++) {
                if(i>=image.getHeight()-1 || j>=image.getWidth()-1 || j <maskSize || i <maskSize)
                    return Color.BLACK;

                blue.add( image.getPixelReader().getColor(j, i).getBlue());
                red.add(image.getPixelReader().getColor(j, i).getRed());
                green.add(image.getPixelReader().getColor(j, i).getGreen());
            }
        }
        Collections.sort(red);
        Collections.sort(green);
        Collections.sort(blue);


        return Color.color(Double.parseDouble(red.get(choice-1).toString()),Double.parseDouble(green.get(choice-1).toString()),Double.parseDouble(blue.get(choice-1).toString()));
    }

}
