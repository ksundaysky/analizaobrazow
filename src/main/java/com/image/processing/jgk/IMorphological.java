import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public interface IMorphological extends PictureUtilities {


    WritableImage otwarcie(int radius, WritableImage writableImage) throws MyException;


    WritableImage gradientMorfologiczny_1(WritableImage obraz) throws MyException;


    WritableImage gradientMorfologiczny_2(WritableImage obraz) throws MyException;


    WritableImage gradientMorfologiczny_3(WritableImage obraz) throws MyException;


}
