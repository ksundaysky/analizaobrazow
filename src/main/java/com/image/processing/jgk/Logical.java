import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The simple Logical class, implementing algorithmical solutions for image processing problems
 *
 * With this class you can do ex. labeling on image (bwlabel - Matlab's naming)
 *
 * <p>Logical logical = new Logical();</p>
 *
 * First upload Image to WrtiableImage object
 *
 * <p>WritableImage image = logical.loadImage("blobs.png");</p>
 *
 * Then execute function
 *
 * <p>image = logical.labeling(image);</p>
 *
 * Voilà!
 *
 *
 * @author  KiboleHutnik
 * @version 1.0
 * @since   2018-05-21
 */

public class Logical extends PictureType implements IMorphological,ILogical,PictureUtilities {
    String exceptionMsg = "Image must be logical...";
    MyException newExc=new MyException(exceptionMsg);
    /**
     * @param wImage       The picture yoi need to label
     * @return The labeled picture as WritableImage object
     */
    public WritableImage labeling(WritableImage wImage) throws MyException// bwlabel
    {
        if (isLogical(wImage) == false) {
            throw newExc;
        }
        WritableImage writableImage = new WritableImage((int)wImage.getWidth(),(int)wImage.getHeight());

        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                writableImage.getPixelWriter().setColor(j, i, wImage.getPixelReader().getColor(j, i));

        int region = 0;
        int height = (int) writableImage.getHeight();
        int width = (int) writableImage.getWidth();
        int pixelsRegion[][] = new int[height][width];
        int picture[][] = pictureToArray(wImage);
        //Color black = new Color(0,0,0,1);
        List<ArrayList> linked = new ArrayList();

        // pierwszy przebieg
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (picture[i][j] != 0)
                {

                    List neighbors = new ArrayList();
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0) neighbors.add(pixelsRegion[i + kx][j + ky]);
                        }
                    }

                    if (neighbors.isEmpty())
                    {
                        ArrayList list = new ArrayList();
                        list.add(region);
                        linked.add(region, list);
                        pixelsRegion[i][j] = region;
                        region++;
                    } else
                    {
                        int minIndex = neighbors.indexOf(Collections.min(neighbors));
                        pixelsRegion[i][j] = (Integer) neighbors.get(minIndex);
                        for (Object n : neighbors)
                            linked.get((Integer) n).addAll(neighbors);
                    }

                }
            }
        }

        // drugi przebieg

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (picture[j][i] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (j + kx < 0 || i + ky < 0 || j + kx > height - 1 || i + ky > width - 1 || (j + kx == 0 && i + ky == 0))
                                continue;
                            else if (pixelsRegion[j + kx][i + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[j + kx][i + ky])
                                    min = pixelsRegion[j + kx][i + ky];
                        }
                    }
                    pixelsRegion[j][i] = min;
                }
            }
        }

        for (int i = width - 1; i >= 0; i--)
        {
            for (int j = height - 1; j >= 0; j--)
            {
                if (picture[j][i] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (j + kx < 0 || i + ky < 0 || j + kx > height - 1 || i + ky > width - 1 || (j + kx == 0 && i + ky == 0))
                                continue;
                            else if (pixelsRegion[j + kx][i + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[j + kx][i + ky])
                                    min = pixelsRegion[j + kx][i + ky];
                        }
                    }
                    pixelsRegion[j][i] = min;
                }
            }
        }


        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (picture[i][j] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[i + kx][j + ky])
                                    min = pixelsRegion[i + kx][j + ky];
                        }
                    }
                    pixelsRegion[i][j] = min;
                }
            }
        }

        for (int i = height - 1; i >= 0; i--)
        {
            for (int j = width - 1; j >= 0; j--)
            {
                if (picture[i][j] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[i + kx][j + ky])
                                    min = pixelsRegion[i + kx][j + ky];
                        }
                    }
                    pixelsRegion[i][j] = min;
                }
            }
        }

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (picture[j][i] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (j + kx < 0 || i + ky < 0 || j + kx > height - 1 || i + ky > width - 1 || (j + kx == 0 && i + ky == 0))
                                continue;
                            else if (pixelsRegion[j + kx][i + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[j + kx][i + ky])
                                    min = pixelsRegion[j + kx][i + ky];
                        }
                    }
                    pixelsRegion[j][i] = min;
                }
            }
        }

        for (int i = width - 1; i >= 0; i--)
        {
            for (int j = height - 1; j >= 0; j--)
            {
                if (picture[j][i] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (j + kx < 0 || i + ky < 0 || j + kx > height - 1 || i + ky > width - 1 || (j + kx == 0 && i + ky == 0))
                                continue;
                            else if (pixelsRegion[j + kx][i + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[j + kx][i + ky])
                                    min = pixelsRegion[j + kx][i + ky];
                        }
                    }
                    pixelsRegion[j][i] = min;
                }
            }
        }

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (picture[i][j] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[i + kx][j + ky])
                                    min = pixelsRegion[i + kx][j + ky];
                        }
                    }
                    pixelsRegion[i][j] = min;
                }
            }
        }

        for (int i = height - 1; i >= 0; i--)
        {
            for (int j = width - 1; j >= 0; j--)
            {
                if (picture[i][j] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[i + kx][j + ky])
                                    min = pixelsRegion[i + kx][j + ky];
                        }
                    }
                    pixelsRegion[i][j] = min;
                }
            }
        }

        // kolorowanie
        List<String> list = colorForLabel(region);
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                writableImage.getPixelWriter().setColor(j, i, Color.web(list.get(pixelsRegion[i][j])));


//        saveImage(writableImage, name);
        return writableImage;
    }

    /**
     * @param obraz       The picture on which you provide morphological gradient
     * @return The morphological gradient picture as WritableImage object
     */
    public WritableImage gradientMorfologiczny_1(WritableImage obraz) throws MyException
    {
        if (isLogical(obraz) == false) {
            throw newExc;
        }

        WritableImage skorodowany;
        WritableImage finalImage = new WritableImage((int)obraz.getWidth(), (int)obraz.getHeight());
        skorodowany = erozjaGradientMorf(obraz);


        for (int i = 0; i < (int) skorodowany.getHeight(); i++)
        {
            for (int j = 0; j < (int) skorodowany.getWidth(); j++)
            {
//                if(!skorodowany.getPixelReader().getColor(j,i).equals(obraz.getPixelReader().getColor(j,i)))
//                {
//                    finalImage.getPixelWriter().setColor(j,i,obraz.getPixelReader().getColor(j,i));
//                }
//                else
//                    finalImage.getPixelWriter().setColor(j,i,Color.BLACK);
                Color tmp = Color.color((obraz.getPixelReader().getColor(j, i).getRed() - skorodowany.getPixelReader().getColor(j, i).getRed()),
                        (obraz.getPixelReader().getColor(j, i).getGreen() - skorodowany.getPixelReader().getColor(j, i).getGreen()),
                        (obraz.getPixelReader().getColor(j, i).getBlue() - skorodowany.getPixelReader().getColor(j, i).getBlue()));
                finalImage.getPixelWriter().setColor(j, i, tmp);

            }
        }
        // saveImage(finalImage,name);
        return finalImage;
    }

    /**
     * @param obraz       The picture on which you provide morphological gradient
     * @return The morphological gradient picture as WritableImage object
     */
    public WritableImage gradientMorfologiczny_2(WritableImage obraz) throws MyException
    {
        if (isLogical(obraz) == false) {
            throw newExc;
        }

        WritableImage zdylatowany;
        WritableImage finalImage = new WritableImage((int)obraz.getWidth(), (int)obraz.getHeight());
        zdylatowany = dylatacjaGradientMorf(obraz);


        for (int i = 0; i < (int) zdylatowany.getHeight(); i++)
        {
            for (int j = 0; j < (int) zdylatowany.getWidth(); j++)
            {

                Color tmp = Color.color((zdylatowany.getPixelReader().getColor(j, i).getRed() - obraz.getPixelReader().getColor(j, i).getRed()),
                        ( zdylatowany.getPixelReader().getColor(j, i).getGreen() - obraz.getPixelReader().getColor(j, i).getGreen() ),
                        ( zdylatowany.getPixelReader().getColor(j, i).getBlue() - obraz.getPixelReader().getColor(j, i).getBlue()));
                finalImage.getPixelWriter().setColor(j, i, tmp);

            }
        }
        //  saveImage(finalImage,name);
        return finalImage;
    }
    /**
     * @param obraz       The picture on which you provide morphological gradient
     * @return The morphological gradient picture as WritableImage object
     */
    public WritableImage gradientMorfologiczny_3(WritableImage obraz)  throws MyException
    {
        if (isLogical(obraz) == false) {
            throw newExc;
        }
        WritableImage zdylatowany, skorodowany;
        WritableImage finalImage = new WritableImage((int)obraz.getWidth(), (int)obraz.getHeight());
        zdylatowany = dylatacjaGradientMorf(obraz);
        skorodowany = erozjaGradientMorf(obraz);

        for (int i = 0; i < (int) zdylatowany.getHeight(); i++)
        {
            for (int j = 0; j < (int) zdylatowany.getWidth(); j++)
            {

                Color tmp = Color.color((zdylatowany.getPixelReader().getColor(j, i).getRed() - skorodowany.getPixelReader().getColor(j, i).getRed()),
                        ( zdylatowany.getPixelReader().getColor(j, i).getGreen() - skorodowany.getPixelReader().getColor(j, i).getGreen() ),
                        ( zdylatowany.getPixelReader().getColor(j, i).getBlue() - skorodowany.getPixelReader().getColor(j, i).getBlue()));
                finalImage.getPixelWriter().setColor(j, i, tmp);

            }
        }
        //  saveImage(finalImage,name);
        return finalImage;
    }

    /**
     * @param writableImage       The picture you need to delete elements from border
     * @return The picture without objects on border as WritableImage object
     */
    public WritableImage deletElementFromBorder(WritableImage writableImage) throws MyException
    {
        if (isLogical(writableImage) == false) {
            throw newExc;
        }
        int[][] obraz = labeling2(writableImage);

        List listRegionsToDelete = new ArrayList();

        for (int i = 0; i < obraz.length; i++)
        {
            for (int j = 0; j < obraz[0].length; j++)
            {
                if (i == 0 || i == obraz.length - 1 || j == 0 || j == obraz[0].length - 1)
                {
                    if (obraz[i][j] > 0 && !listRegionsToDelete.contains(obraz[i][j]))
                    {
                        listRegionsToDelete.add(obraz[i][j]);
                    }
                }
            }
        }

        for (int i = 0; i < obraz.length; i++)
        {
            for (int j = 0; j < obraz[0].length; j++)
            {
                if (listRegionsToDelete.contains(obraz[i][j]))
                    obraz[i][j] = 0;
                else if (obraz[i][j] != 0)
                {
                    obraz[i][j] = 1;
                }
            }
        }

        WritableImage writableImage1 = new WritableImage(obraz[0].length, obraz.length);

        for (int j = 0; j < obraz.length; j++)
        {
            for (int i = 0; i < obraz[0].length; i++)
            {
                if (obraz[j][i] == 1)
                    writableImage1.getPixelWriter().setColor(i, j, Color.WHITE);
                else
                    writableImage1.getPixelWriter().setColor(i, j, Color.BLACK);
            }
        }

        // saveImage(writableImage1, name);
        return writableImage1;
    }

    /**
     * @param writableImage       The picture on which you provide opening by circle
     * @param  radius       Value of circle's radius
     * @return Opened by circle picture as WritableImage object
     */
    public WritableImage otwarcie(int radius, WritableImage writableImage) throws MyException
    {
        if (isLogical(writableImage) == false) {
            throw newExc;
        }

        //int[][] obraz = pictureToArray(writableImage);
        //int[][] picture = erozja(radius, obraz);
        WritableImage picture = erozja(3, writableImage);
        picture = dylatacja(radius, picture);
        return picture;
        //WritableImage wImage = arrayToBinaryPicture(picture);
        // saveImage(picture, name);

    }

    private int[][] labeling2(WritableImage wImage) throws MyException// bwlabel
    {
        WritableImage writableImage = new WritableImage((int)wImage.getWidth(),(int)wImage.getHeight());

        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                writableImage.getPixelWriter().setColor(j, i, wImage.getPixelReader().getColor(j, i));

        int region = 0;
        int height = (int) writableImage.getHeight();
        int width = (int) writableImage.getWidth();
        int pixelsRegion[][] = new int[height][width];
        int picture[][] = pictureToArray(wImage);
        //Color black = new Color(0,0,0,1);
        List<ArrayList> linked = new ArrayList();

        // pierwszy przebieg
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (picture[i][j] != 0)
                {

                    List neighbors = new ArrayList();
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0) neighbors.add(pixelsRegion[i + kx][j + ky]);
                        }
                    }

                    if (neighbors.isEmpty())
                    {
                        ArrayList list = new ArrayList();
                        list.add(region);
                        linked.add(region, list);
                        pixelsRegion[i][j] = region;
                        region++;
                    } else
                    {
                        int minIndex = neighbors.indexOf(Collections.min(neighbors));
                        pixelsRegion[i][j] = (Integer) neighbors.get(minIndex);
                        for (Object n : neighbors)
                            linked.get((Integer) n).addAll(neighbors);
                    }

                }
            }
        }

        // drugi przebieg

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (picture[j][i] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (j + kx < 0 || i + ky < 0 || j + kx > height - 1 || i + ky > width - 1 || (j + kx == 0 && i + ky == 0))
                                continue;
                            else if (pixelsRegion[j + kx][i + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[j + kx][i + ky])
                                    min = pixelsRegion[j + kx][i + ky];
                        }
                    }
                    pixelsRegion[j][i] = min;
                }
            }
        }

        for (int i = width - 1; i >= 0; i--)
        {
            for (int j = height - 1; j >= 0; j--)
            {
                if (picture[j][i] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (j + kx < 0 || i + ky < 0 || j + kx > height - 1 || i + ky > width - 1 || (j + kx == 0 && i + ky == 0))
                                continue;
                            else if (pixelsRegion[j + kx][i + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[j + kx][i + ky])
                                    min = pixelsRegion[j + kx][i + ky];
                        }
                    }
                    pixelsRegion[j][i] = min;
                }
            }
        }


        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (picture[i][j] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[i + kx][j + ky])
                                    min = pixelsRegion[i + kx][j + ky];
                        }
                    }
                    pixelsRegion[i][j] = min;
                }
            }
        }

        for (int i = height - 1; i >= 0; i--)
        {
            for (int j = width - 1; j >= 0; j--)
            {
                if (picture[i][j] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[i + kx][j + ky])
                                    min = pixelsRegion[i + kx][j + ky];
                        }
                    }
                    pixelsRegion[i][j] = min;
                }
            }
        }

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (picture[j][i] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (j + kx < 0 || i + ky < 0 || j + kx > height - 1 || i + ky > width - 1 || (j + kx == 0 && i + ky == 0))
                                continue;
                            else if (pixelsRegion[j + kx][i + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[j + kx][i + ky])
                                    min = pixelsRegion[j + kx][i + ky];
                        }
                    }
                    pixelsRegion[j][i] = min;
                }
            }
        }

        for (int i = width - 1; i >= 0; i--)
        {
            for (int j = height - 1; j >= 0; j--)
            {
                if (picture[j][i] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (j + kx < 0 || i + ky < 0 || j + kx > height - 1 || i + ky > width - 1 || (j + kx == 0 && i + ky == 0))
                                continue;
                            else if (pixelsRegion[j + kx][i + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[j + kx][i + ky])
                                    min = pixelsRegion[j + kx][i + ky];
                        }
                    }
                    pixelsRegion[j][i] = min;
                }
            }
        }

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (picture[i][j] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[i + kx][j + ky])
                                    min = pixelsRegion[i + kx][j + ky];
                        }
                    }
                    pixelsRegion[i][j] = min;
                }
            }
        }

        for (int i = height - 1; i >= 0; i--)
        {
            for (int j = width - 1; j >= 0; j--)
            {
                if (picture[i][j] != 0)
                {
                    int min = Integer.MAX_VALUE;
                    for (int kx = -1; kx <= 1; kx++)
                    {    // zapisuje wartosc region�w-sasiad�w
                        for (int ky = -1; ky <= 1; ky++)
                        {
                            if (i + kx < 0 || j + ky < 0 || i + kx > height - 1 || j + ky > width - 1 || (i + kx == 0 && j + ky == 0))
                                continue;
                            else if (pixelsRegion[i + kx][j + ky] != 0)
                                //neighbors.add(pixelsRegion[i + kx][j + ky]);
                                if (min > pixelsRegion[i + kx][j + ky])
                                    min = pixelsRegion[i + kx][j + ky];
                        }
                    }
                    pixelsRegion[i][j] = min;
                }
            }
        }

        return pixelsRegion;
    }

    private String minimumColor(int radius, int x0, int y0, WritableImage writableImage)
    {
        int minimum = Integer.MAX_VALUE;
        for (int i = x0 - radius; i <= x0 + radius; i++)
            for (int j = y0 - radius; j <= y0 + radius; j++)
                if ((i - x0) * (i - x0) + (j - y0) * (j - y0) <= radius * radius)
                {
                    if (i < 0 || i >= (int) writableImage.getHeight() || j < 0 || j >= (int) writableImage.getWidth())
                        continue;

                    int temp = NumberSystems.hexToDec(writableImage.getPixelReader().getColor(j, i).toString());
                    if (temp < minimum)
                        minimum = temp;
                    if (minimum == 0) return NumberSystems.decToHex(minimum);
                }
        //System.out.println(minimum);
        return NumberSystems.decToHex(minimum);
    }

    private String maximumColor(int radius, int x0, int y0, WritableImage writableImage)
    {
        int maximum = Integer.MIN_VALUE;
        for (int i = x0 - radius; i <= x0 + radius; i++)
            for (int j = y0 - radius; j <= y0 + radius; j++)
                if ((i - x0) * (i - x0) + (j - y0) * (j - y0) <= radius * radius)
                {
                    if (i < 0 || i >= (int) writableImage.getHeight() || j < 0 || j >= (int) writableImage.getWidth())
                        continue;

                    int temp = NumberSystems.hexToDec(writableImage.getPixelReader().getColor(j, i).toString());
                    if (temp > maximum)
                        maximum = temp;
                    if (maximum == 16777215) return NumberSystems.decToHex(maximum);
                }
        //System.out.println(minimum);
        return NumberSystems.decToHex(maximum);
    }

    private WritableImage dylatacja(int radius, WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());
        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                w.getPixelWriter().setColor(j, i, writableImage.getPixelReader().getColor(j, i));

        for (int k = 0; k < (int) writableImage.getHeight(); k++)
        {
            for (int l = 0; l < (int) writableImage.getWidth(); l++)
            {
                w.getPixelWriter().setColor(l, k, Color.web("#" + maximumColor(radius, k, l, writableImage)));
            }
        }

        return w;
    }

    private WritableImage erozja(int radius, WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());

        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                w.getPixelWriter().setColor(j, i, writableImage.getPixelReader().getColor(j, i));

        for (int k = 0; k < (int) writableImage.getHeight(); k++)
        {
            for (int l = 0; l < (int) writableImage.getWidth(); l++)
            {
                w.getPixelWriter().setColor(l, k, Color.web("#" + minimumColor(radius, k, l, writableImage)));
            }
        }

        return w;
    }

    private List<String> colorForLabel(int howManyRegions)
    {
        int space = 16777215 / (howManyRegions - 1);
        List<String> list = new ArrayList();

        for (int i = 0; i < 16777214; i += space)
            list.add("#" + NumberSystems.decToHex(i));
        list.add("#ffffff");

        for (String s : list)
            System.out.println(s);
        return list;
    }

    private String minimumColorGradientMorf(int x0, int y0, WritableImage writableImage)
    {
        int minimum = Integer.MAX_VALUE;
        for (int i = x0 - 1; i <= x0 + 1; i++)
            for (int j = y0 - 1; j <= y0 + 1; j++)
            {
                if (i < 0 || i >= (int) writableImage.getHeight() || j < 0 || j >= (int) writableImage.getWidth())
                    continue;

                int temp = NumberSystems.hexToDec(writableImage.getPixelReader().getColor(j, i).toString());
                if (temp < minimum)
                    minimum = temp;
                if (minimum == 0) return NumberSystems.decToHex(minimum);
            }
        //System.out.println(minimum);
        return NumberSystems.decToHex(minimum);
    }

    private String maximumColorGradientMorf(int x0, int y0, WritableImage writableImage)
    {
        int maximum = Integer.MIN_VALUE;
        for (int i = x0 - 1; i <= x0 + 1; i++)
            for (int j = y0 - 1; j <= y0 + 1; j++)
            {
                if (i < 0 || i >= (int) writableImage.getHeight() || j < 0 || j >= (int) writableImage.getWidth())
                    continue;

                int temp = NumberSystems.hexToDec(writableImage.getPixelReader().getColor(j, i).toString());
                if (temp > maximum)
                    maximum = temp;
                if (maximum == 16777215) return NumberSystems.decToHex(maximum);
            }
        //System.out.println(minimum);
        return NumberSystems.decToHex(maximum);
    }

    private WritableImage dylatacjaGradientMorf(WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());
        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                w.getPixelWriter().setColor(j, i, writableImage.getPixelReader().getColor(j, i));

        for (int k = 0; k < (int) writableImage.getHeight(); k++)
        {
            for (int l = 0; l < (int) writableImage.getWidth(); l++)
            {
                w.getPixelWriter().setColor(l, k, Color.web("#" + maximumColorGradientMorf(k, l, writableImage)));
            }
        }
        return w;
    }

    private WritableImage erozjaGradientMorf( WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());

        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                w.getPixelWriter().setColor(j, i, writableImage.getPixelReader().getColor(j, i));

        for (int k = 0; k < (int) writableImage.getHeight(); k++)
        {
            for (int l = 0; l < (int) writableImage.getWidth(); l++)
            {
                w.getPixelWriter().setColor(l, k, Color.web("#" + minimumColorGradientMorf(k, l, writableImage)));
            }
        }

        return w;
    }

}
