import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;

public interface ISpatialFiltration extends PictureUtilities {

      WritableImage stdfilt(WritableImage writableImage, int[][] tab) throws MyException;

      WritableImage ordfilt2(WritableImage image, int choice, int maskSize) throws MyException;

}
