import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public abstract class PictureType
{
    public boolean isLogical(WritableImage image)
    {
        for( int i = 0 ; i< image.getHeight(); i++)
        {
            for( int j = 0; j< image.getWidth();j++)
            {
                if (image.getPixelReader().getColor(j,i).equals(Color.BLACK) ||  image.getPixelReader().getColor(j,i).equals(Color.WHITE));

                else return false;
            }
        }
        return true;
    }

    public boolean isMono(WritableImage image){
        if(isLogical(image))
            return false;
        int flagaR = 0;
        int flagaG = 0;
        int flagaB = 0;
        for( int i = 0 ; i< image.getHeight(); i++)
        {
            for (int j = 0; j < image.getWidth(); j++)
            {
                double red = image.getPixelReader().getColor(j, i).getRed();
                double green = image.getPixelReader().getColor(j, i).getGreen();
                double blue = image.getPixelReader().getColor(j, i).getBlue();
                if (red != 0)
                    flagaR = 1;
                if (green != 0)
                    flagaG = 1;
                if (blue != 0)
                    flagaB = 1;
            }
        }

        if(flagaR==1 && flagaG ==1 && flagaB==1)
        {
            for( int i = 0 ; i< image.getHeight(); i++)
            {
                for (int j = 0; j < image.getWidth(); j++)
                {
                    double red = image.getPixelReader().getColor(j, i).getRed();
                    double green = image.getPixelReader().getColor(j, i).getGreen();
                    double blue = image.getPixelReader().getColor(j, i).getBlue();
                    if(red!=green || red!=blue || blue!=green)
                        return false;
                }
            }
            return true;

        }
        else if(flagaR==1 && flagaG ==0 && flagaB==0)
            return true;
        else if(flagaR==0 && flagaG ==1 && flagaB==0)
            return true;
        else if(flagaR==0 && flagaG ==0 && flagaB==1)
            return true;


        return false;
    }

    public boolean isRGB(WritableImage image){

        if(isMono(image) || isLogical(image))
            return false;
        return true;
    }
}
