import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface ILogical extends PictureUtilities {

      WritableImage labeling(WritableImage wImage) throws MyException;

      WritableImage deletElementFromBorder(WritableImage writableImage) throws MyException;

}
