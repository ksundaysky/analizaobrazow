import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;


/**
 * The simple Morphological class, implementing algorithmical solutions for image processing problems
 *
 * With this class you can do for ex. opennig by circle on image
 *
 * <p>Monochrome monochrome = new Monochrome();</p>
 *
 * First upload Image to WrtiableImage object
 *
 * <p>WritableImage image = monochrome.loadImage("blobs.png");</p>
 *
 * Then execute function
 *
 * <p>image = monochrome.otwarcie(3,image);</p>
 *
 * Voilà!
 *
 *
 * @author  KiboleHutnik
 * @version 1.0
 * @since   2018-05-21
 */

public class Monochrome extends PictureType implements IMorphological, ISpatialFiltration,PictureUtilities {
    String exceptionMsg = "Image must be monochrome...";
    MyException newExc=new MyException(exceptionMsg);
    /**
     *  2-D order-statistic filtering
     * @param image       The picture on which you provide order-statistic filtering
     * @param choice Element to replace the target picture
     * @param maskSize The size of numeric matrix
     * @return Ordered-statistics filtering picture as WritableImage object
     */
    public WritableImage ordfilt2(WritableImage image, int choice, int maskSize) throws MyException
    {
        if (isMono(image) == false) {
            throw newExc;
        }
        if(choice > maskSize*maskSize)
        {
            System.out.println("Please provide valid number");
            return null;
        }
        WritableImage writableImage = new WritableImage((int)image.getWidth(),(int)image.getHeight());

        for(int i = 0;i<writableImage.getHeight();i++)
        {
            for(int j =0;j<writableImage.getWidth();j++)
            {
                writableImage.getPixelWriter().setColor(j,i,sortAndChose(image,choice,maskSize,i,j));
            }
        }

        //saveImage(writableImage,name);

        return writableImage;
    }

    /**
     *      Morphological Gradient
     * @param obraz       The picture on which you provide morphological gradient
     * @return The morphological gradient picture as WritableImage object
     */
    public WritableImage gradientMorfologiczny_3(WritableImage obraz) throws MyException
    {

        if (isMono(obraz) == false) {
            throw newExc;
        }
        WritableImage zdylatowany, skorodowany;
        WritableImage finalImage = new WritableImage((int)obraz.getWidth(), (int)obraz.getHeight());
        zdylatowany = dylatacjaGradientMorf(obraz);
        skorodowany = erozjaGradientMorf(obraz);

        for (int i = 0; i < (int) zdylatowany.getHeight(); i++)
        {
            for (int j = 0; j < (int) zdylatowany.getWidth(); j++)
            {

                Color tmp = Color.color((zdylatowany.getPixelReader().getColor(j, i).getRed() - skorodowany.getPixelReader().getColor(j, i).getRed()),
                        ( zdylatowany.getPixelReader().getColor(j, i).getGreen() - skorodowany.getPixelReader().getColor(j, i).getGreen() ),
                        ( zdylatowany.getPixelReader().getColor(j, i).getBlue() - skorodowany.getPixelReader().getColor(j, i).getBlue()));
                finalImage.getPixelWriter().setColor(j, i, tmp);

            }
        }
        //  saveImage(finalImage,name);
        return finalImage;
    }

    /**
     * Opening picture by circle
     * @param writableImage       The picture on which you provide opening by circle
     * @param  radius       Value of circle's radius
     * @return Opened by circle picture as WritableImage object
     */
    public WritableImage otwarcie(int radius, WritableImage writableImage) throws MyException
    {
        if (isMono(writableImage) == false) {
            throw newExc;
        }
        //int[][] obraz = pictureToArray(writableImage);
        //int[][] picture = erozja(radius, obraz);
        WritableImage picture = erozja(3, writableImage);
        picture = dylatacja(radius, picture);
        return picture;
        //WritableImage wImage = arrayToBinaryPicture(picture);
        // saveImage(picture, name);

    }

    /**
     *      Local standard deviation of image
     * @param writableImage       The picture on which you provide standard deviation
     * @param tab Multidimensional, logical, or numeric array containing zeros and ones
     * @return Local standard deviation picture as WritableImage object
     */
    public WritableImage stdfilt(WritableImage writableImage, int[][] tab) throws MyException
    {
        if (isMono(writableImage) == false) {
            throw newExc;
        }
        if(tab.length%2 == 0 || tab[0].length != tab.length) return null;
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());

        for (int i = tab.length/2; i < (int) writableImage.getHeight()-(tab.length/2); i++)
            for (int j = tab.length/2; j < (int) writableImage.getWidth()-(tab.length/2); j++)
                w.getPixelWriter().setColor(j, i, std(writableImage, tab, i, j));

        w = normalization(w);
        //saveImage(w, name);

        return w;
    }

    /**
     * Morphological Gradient
     * @param obraz       The picture on which you provide morphological gradient
     * @return The morphological gradient picture as WritableImage object
     */
    public WritableImage gradientMorfologiczny_1(WritableImage obraz) throws MyException
    {
        if (isMono(obraz) == false) {
            throw newExc;
        }
        WritableImage skorodowany;
        WritableImage finalImage = new WritableImage((int)obraz.getWidth(), (int)obraz.getHeight());
        skorodowany = erozjaGradientMorf(obraz);


        for (int i = 0; i < (int) skorodowany.getHeight(); i++)
        {
            for (int j = 0; j < (int) skorodowany.getWidth(); j++)
            {
//                if(!skorodowany.getPixelReader().getColor(j,i).equals(obraz.getPixelReader().getColor(j,i)))
//                {
//                    finalImage.getPixelWriter().setColor(j,i,obraz.getPixelReader().getColor(j,i));
//                }
//                else
//                    finalImage.getPixelWriter().setColor(j,i,Color.BLACK);
                Color tmp = Color.color((obraz.getPixelReader().getColor(j, i).getRed() - skorodowany.getPixelReader().getColor(j, i).getRed()),
                        (obraz.getPixelReader().getColor(j, i).getGreen() - skorodowany.getPixelReader().getColor(j, i).getGreen()),
                        (obraz.getPixelReader().getColor(j, i).getBlue() - skorodowany.getPixelReader().getColor(j, i).getBlue()));
                finalImage.getPixelWriter().setColor(j, i, tmp);

            }
        }
        // saveImage(finalImage,name);
        return finalImage;
    }


    /**
     *  Morphological Gradient
     * @param obraz       The picture on which you provide morphological gradient
     * @return The morphological gradient picture as WritableImage object
     */
    public WritableImage gradientMorfologiczny_2(WritableImage obraz) throws MyException
    {
        if (isMono(obraz) == false) {
            throw newExc;
        }
        WritableImage zdylatowany;
        WritableImage finalImage = new WritableImage((int)obraz.getWidth(), (int)obraz.getHeight());
        zdylatowany = dylatacjaGradientMorf(obraz);


        for (int i = 0; i < (int) zdylatowany.getHeight(); i++)
        {
            for (int j = 0; j < (int) zdylatowany.getWidth(); j++)
            {

                Color tmp = Color.color((zdylatowany.getPixelReader().getColor(j, i).getRed() - obraz.getPixelReader().getColor(j, i).getRed()),
                        ( zdylatowany.getPixelReader().getColor(j, i).getGreen() - obraz.getPixelReader().getColor(j, i).getGreen() ),
                        ( zdylatowany.getPixelReader().getColor(j, i).getBlue() - obraz.getPixelReader().getColor(j, i).getBlue()));
                finalImage.getPixelWriter().setColor(j, i, tmp);

            }
        }
        //  saveImage(finalImage,name);
        return finalImage;
    }

    private String minimumColor(int radius, int x0, int y0, WritableImage writableImage)
    {
        int minimum = Integer.MAX_VALUE;
        for (int i = x0 - radius; i <= x0 + radius; i++)
            for (int j = y0 - radius; j <= y0 + radius; j++)
                if ((i - x0) * (i - x0) + (j - y0) * (j - y0) <= radius * radius)
                {
                    if (i < 0 || i >= (int) writableImage.getHeight() || j < 0 || j >= (int) writableImage.getWidth())
                        continue;

                    int temp = NumberSystems.hexToDec(writableImage.getPixelReader().getColor(j, i).toString());
                    if (temp < minimum)
                        minimum = temp;
                    if (minimum == 0) return NumberSystems.decToHex(minimum);
                }
        //System.out.println(minimum);
        return NumberSystems.decToHex(minimum);
    }

    private String maximumColor(int radius, int x0, int y0, WritableImage writableImage)
    {
        int maximum = Integer.MIN_VALUE;
        for (int i = x0 - radius; i <= x0 + radius; i++)
            for (int j = y0 - radius; j <= y0 + radius; j++)
                if ((i - x0) * (i - x0) + (j - y0) * (j - y0) <= radius * radius)
                {
                    if (i < 0 || i >= (int) writableImage.getHeight() || j < 0 || j >= (int) writableImage.getWidth())
                        continue;

                    int temp = NumberSystems.hexToDec(writableImage.getPixelReader().getColor(j, i).toString());
                    if (temp > maximum)
                        maximum = temp;
                    if (maximum == 16777215) return NumberSystems.decToHex(maximum);
                }
        //System.out.println(minimum);
        return NumberSystems.decToHex(maximum);
    }

    private WritableImage dylatacja(int radius, WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());
        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                w.getPixelWriter().setColor(j, i, writableImage.getPixelReader().getColor(j, i));

        for (int k = 0; k < (int) writableImage.getHeight(); k++)
        {
            for (int l = 0; l < (int) writableImage.getWidth(); l++)
            {
                w.getPixelWriter().setColor(l, k, Color.web("#" + maximumColor(radius, k, l, writableImage)));
            }
        }

        return w;
    }

    private WritableImage erozja(int radius, WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());

        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                w.getPixelWriter().setColor(j, i, writableImage.getPixelReader().getColor(j, i));

        for (int k = 0; k < (int) writableImage.getHeight(); k++)
        {
            for (int l = 0; l < (int) writableImage.getWidth(); l++)
            {
                w.getPixelWriter().setColor(l, k, Color.web("#" + minimumColor(radius, k, l, writableImage)));
            }
        }

        return w;
    }
    private String minimumColorGradientMorf(int x0, int y0, WritableImage writableImage)
    {
        int minimum = Integer.MAX_VALUE;
        for (int i = x0 - 1; i <= x0 + 1; i++)
            for (int j = y0 - 1; j <= y0 + 1; j++)
            {
                if (i < 0 || i >= (int) writableImage.getHeight() || j < 0 || j >= (int) writableImage.getWidth())
                    continue;

                int temp = NumberSystems.hexToDec(writableImage.getPixelReader().getColor(j, i).toString());
                if (temp < minimum)
                    minimum = temp;
                if (minimum == 0) return NumberSystems.decToHex(minimum);
            }
        //System.out.println(minimum);
        return NumberSystems.decToHex(minimum);
    }

    private String maximumColorGradientMorf(int x0, int y0, WritableImage writableImage)
    {
        int maximum = Integer.MIN_VALUE;
        for (int i = x0 - 1; i <= x0 + 1; i++)
            for (int j = y0 - 1; j <= y0 + 1; j++)
            {
                if (i < 0 || i >= (int) writableImage.getHeight() || j < 0 || j >= (int) writableImage.getWidth())
                    continue;

                int temp = NumberSystems.hexToDec(writableImage.getPixelReader().getColor(j, i).toString());
                if (temp > maximum)
                    maximum = temp;
                if (maximum == 16777215) return NumberSystems.decToHex(maximum);
            }
        //System.out.println(minimum);
        return NumberSystems.decToHex(maximum);
    }

    private WritableImage dylatacjaGradientMorf(WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());
        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                w.getPixelWriter().setColor(j, i, writableImage.getPixelReader().getColor(j, i));

        for (int k = 0; k < (int) writableImage.getHeight(); k++)
        {
            for (int l = 0; l < (int) writableImage.getWidth(); l++)
            {
                w.getPixelWriter().setColor(l, k, Color.web("#" + maximumColorGradientMorf(k, l, writableImage)));
            }
        }
        return w;
    }

    private WritableImage erozjaGradientMorf( WritableImage writableImage)
    {
        WritableImage w = new WritableImage((int) writableImage.getWidth(), (int) writableImage.getHeight());

        for (int i = 0; i < (int) writableImage.getHeight(); i++)
            for (int j = 0; j < (int) writableImage.getWidth(); j++)
                w.getPixelWriter().setColor(j, i, writableImage.getPixelReader().getColor(j, i));

        for (int k = 0; k < (int) writableImage.getHeight(); k++)
        {
            for (int l = 0; l < (int) writableImage.getWidth(); l++)
            {
                w.getPixelWriter().setColor(l, k, Color.web("#" + minimumColorGradientMorf(k, l, writableImage)));
            }
        }

        return w;
    }

    private Color std(WritableImage writableImage, int[][] mask, int x0, int y0)
    {
        Color tmp;
        int size = mask.length;
        double n = 0.0;
        double sredniaBlue = 0;
        double sredniaRed = 0;
        double sredniaGreen = 0;
        int x = 0;
        int y = 0;
        for (int i = x0 - size/2; i < x0 + size/2; i++) {
            for (int j = y0 - size / 2; j < y0 + size / 2; j++) {

                if(mask[y][x] >= 1) {
                    sredniaBlue += writableImage.getPixelReader().getColor(j, i).getBlue();
                    sredniaRed += writableImage.getPixelReader().getColor(j, i).getRed();
                    sredniaGreen += writableImage.getPixelReader().getColor(j, i).getGreen();
                    n++;

                }
                x++;
            }
            y++;
            x=0;
        }
        sredniaBlue /= n;
        sredniaRed /= n;
        sredniaGreen /= n;

        double skladowaRed = 0;
        double skladowaGreen = 0;
        double skladowaBlue = 0;
        for (int i = x0 - size/2; i <= x0 + size/2; i++) {
            for (int j = y0 - size / 2; j <= y0 + size / 2; j++) {
                skladowaRed += Math.pow(writableImage.getPixelReader().getColor(j, i).getRed() - sredniaRed, 2);
                skladowaGreen += Math.pow(writableImage.getPixelReader().getColor(j, i).getGreen() - sredniaGreen, 2);
                skladowaBlue += Math.pow(writableImage.getPixelReader().getColor(j, i).getBlue() - sredniaBlue, 2);
            }
        }
        skladowaBlue /= (n-1);
        skladowaRed /= (n-1);
        skladowaGreen /= (n-1);

        skladowaBlue = Math.sqrt(skladowaBlue);
        skladowaRed = Math.sqrt(skladowaRed);
        skladowaGreen = Math.sqrt(skladowaGreen);

        if(skladowaBlue > 1) skladowaBlue = 1.0;
        if(skladowaGreen > 1) skladowaGreen = 1.0;
        if(skladowaRed > 1) skladowaRed = 1.0;
        tmp = Color.color(skladowaRed, skladowaGreen, skladowaBlue);

        return tmp;
    }

    private WritableImage normalization(WritableImage writableImage) {
        WritableImage w = new WritableImage((int)writableImage.getWidth(), (int)writableImage.getHeight());
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < (int) writableImage.getHeight(); i++){
            for (int j = 0; j < (int) writableImage.getWidth(); j++) {
                if(min > writableImage.getPixelReader().getColor(j, i).getBlue())
                    min = writableImage.getPixelReader().getColor(j, i).getBlue();
                if(min > writableImage.getPixelReader().getColor(j, i).getRed())
                    min = writableImage.getPixelReader().getColor(j, i).getRed();
                if(min > writableImage.getPixelReader().getColor(j, i).getGreen())
                    min = writableImage.getPixelReader().getColor(j, i).getGreen();

                if(max < writableImage.getPixelReader().getColor(j, i).getBlue())
                    max = writableImage.getPixelReader().getColor(j, i).getBlue();
                if(max < writableImage.getPixelReader().getColor(j, i).getRed())
                    max = writableImage.getPixelReader().getColor(j, i).getRed();
                if(max < writableImage.getPixelReader().getColor(j, i).getGreen())
                    max = writableImage.getPixelReader().getColor(j, i).getGreen();
            }
        }

        double wsp = 1.0/(max-min);

        for (int i = 0; i < (int) writableImage.getHeight(); i++) {
            for (int j = 0; j < (int) writableImage.getWidth(); j++) {
                Color color = Color.color((writableImage.getPixelReader().getColor(j, i).getRed()-min)*wsp,
                        (writableImage.getPixelReader().getColor(j, i).getGreen()-min)*wsp,
                        (writableImage.getPixelReader().getColor(j, i).getBlue()-min)*wsp);
                w.getPixelWriter().setColor(j,i,color);
            }
        }

        return w;
    }

    private Color sortAndChose(WritableImage image,int choice, int maskSize,int x0,int y0)
    {
        ArrayList red =new  ArrayList();
        ArrayList green =new  ArrayList();
        ArrayList blue =new  ArrayList();

        for (int i = x0; i < x0 + maskSize; i++) {
            for (int j = y0 ; j < y0 + maskSize; j++) {
                if(i>=image.getHeight()-1 || j>=image.getWidth()-1 || j <maskSize || i <maskSize)
                    return Color.BLACK;

                blue.add( image.getPixelReader().getColor(j, i).getBlue());
                red.add(image.getPixelReader().getColor(j, i).getRed());
                green.add(image.getPixelReader().getColor(j, i).getGreen());
            }
        }
        Collections.sort(red);
        Collections.sort(green);
        Collections.sort(blue);


        return Color.color(Double.parseDouble(red.get(choice-1).toString()),Double.parseDouble(green.get(choice-1).toString()),Double.parseDouble(blue.get(choice-1).toString()));
    }

}
